# Moteur d'inférence 0+

## Contexte

Un moteur d'inférence 0+ réalisé dans le cadre de l'UE "Intelligence Artificielle" en master 1 informatique fait par Romain Grelier et Romane Poirier.
Ce moteur comporte la possibilité de créer des attributs et leur donner des valeurs, ces valeurs pourront ensuite être comparé lors de l'inférence.
Le domaine d'application choisit est la gestion de promotion d'une entreprise en fonction du salaire, poste et ancienneté de ses employés.

## Prérequis

Le moteur d'inférence a été réalisé en Julia (v1.0.1), il peut donc être nécessaire de télécharger l'interprêteur sur le site [julialang.org]

## Utilisation

```sh
$ julia InferenceEngine.jl filename algorithm policy saturate
```

- filename : nom du fichier avec la base de connaissance, liste des faits et but
- algorithm : (forward/backward)
- policy : façon de choisir une règle durant l'algorithme (premiseCount/resultCout/random/firstRule)
- saturate : saturer ou non la list de fait (true/false)

### Language

Il est possible de donner directement un fichier au moteur contenant la base de connaissance, la liste des faits ainsi que le but.
Chaque "Element" s'écrit en trois parties :
  - variable : il s'agit du nom de la variable
  - opérateur : le moteur gère les opérateurs <, >, =, <=, >=, <>
  - valeur : il peut s'agir de chaine de caractère (seuls l'égalité est utilisable), ou de valeurs numériques (tous les opérateurs sont utilisables)

L'opérateur "->" permet d'indiquer que ce qui suit est la conclusion d'une règle
L'opérateur "&" indique une prémisse supplémentaire ou un résultat supplémentaire dans un règle

Un fichier se décompose en trois parties, chacune séparée par une ligne vide, dans l'ordre suivant:
  - Base de connaissance (une règle par ligne)
  - Liste des faits (un fait par ligne)
  - But (si besoin)

Voici un exemple de fichier valide (d'autres fichiers exemple sont disponibles dans le répertoire):

> salaire > 1500 & temps < 3 -> poste = apprenti
> salaire > 1500 & temps > 3 -> poste = dev
> temps > 5 -> salaire = 2000
> salaire > 3000 -> poste = dev
> diplome = master -> poste = apprenti
> diplome = master & temps > 3 -> salaire = 1500

> diplome = master
> temps = 10

> poste = dev
