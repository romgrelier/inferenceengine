include("Rule.jl")

# Knowledge Base
KnowledgeBase = Set{Rule}
# Facts
Facts = Set{Fact}
# Goal
Goals = Set{Goal}
