include("Util.jl")

function forwardChaining(knowledgeBase::KnowledgeBase, facts::Facts, goals::Goals, saturate::Bool, policy::ConflictPolicy)
    notFinished = true

    # check if any goal can be validated
    for goal in goals
        for fact in facts
            if isPremiseValid(goal, fact)
                println("[goal reached : $(goal.variable)]")
                delete!(goals, goal)
                break
            end
        end
        if !saturate && length(goals) == 0
            return true
        end
    end

    iteration::Int64 = 0
    while notFinished # if we need to do another pass through the rules
        notFinished = false
        iteration += 1

        println("[$(iteration)]")

        print("|\tfacts : ")
        for fact in facts
            print(fact)
        end
        println()

        # get available rules
        validRules = listValidRules(knowledgeBase, facts)
        if length(validRules) == 0
            println("no rule available -STOP-")
            return false
        end
        println("|\tavailable rules :")
        for rule in validRules
            print("|\t\t")
            println(rule)
        end

        if length(validRules) >= 1
            notFinished = true

            # apply policy in case of conflict
            choosenRule = chooseRuleToApply(validRules, policy)
            print("|\tchoosen rule : ")
            println(choosenRule)

            # add new facts from the chossen rule
            for fact in choosenRule.result
                # add new facts
                push!(facts, fact)

                # check goals
                for goal in goals
                    if isPremiseValid(goal, fact)
                        println("[goal reached : $(goal.variable)]")
                        delete!(goals, goal)
                        break
                    end
                end
                if !saturate && length(goals) == 0
                    return true
                end

            end
            # remove the rule from the knowledge Base
            delete!(knowledgeBase, choosenRule)
        end

        println()
    end

    return facts

end
