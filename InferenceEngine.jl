module InferenceEngine

    export loadFromFile
    export forwardChaining
    export backwardChaining
    export ConflictPolicy

    include("FileLoader.jl")
    include("ForwardChaining.jl")
    include("BackwardChaining.jl")
    include("Util.jl")

end

if !(length(ARGS) == 4)
    println("Usage : ")
    println("\tjulia InferenceEngine.jl filename forward policy saturate")
    println("= algorithm =")
    println("\tforward : chaînage avant")
    println("\tbackward : chaîage arrière")
    println("= policy =")
    println("\tfirstRule : première règle disponible")
    println("\tpremiseCount : nombre de premise")
    println("\tresultCount : nombre de conclusion")
    println("\trandom : règle aléatoire")
    println("= saturate =")
    println("\ttrue : saturer la base de fait")
    println("\tfalse : ne pas saturer la base de fait")
    exit()
end

filename = ARGS[1]
kb, facts, goals = InferenceEngine.loadFromFile(filename)

policy = InferenceEngine.firstRule

if ARGS[3] == "firstRule"
elseif ARGS[3] == "premiseCout"
elseif ARGS[3] == "resultCout"
elseif ARGS[3] == "random"
end

saturate = false
if ARGS[4] == "true"
    saturate = true
end

if ARGS[2] == "forward"
    InferenceEngine.forwardChaining(kb, facts, goals, saturate, policy)
elseif ARGS[2] == "backward"
    InferenceEngine.backwardChaining(kb, facts, goals, saturate, policy)
end
