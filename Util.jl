include("KnowledgeBaseType.jl")

@enum ConflictPolicy begin
    premiseEvaluationComplexity
    premiseCount
    resultCount
    firstRule
    random
end

function chooseRuleToApply(listRules::Set{Rule}, policy::ConflictPolicy)::Rule
    temp::Array{Rule, 1} = [rule for rule in listRules]

    if policy == premiseEvaluationComplexity
        return temp[1]
    elseif policy == premiseCount
        for rule in rules
            selectedRule = temp[1]
            if length(rule.premise) > length(selectedRule.premise)
                selectedRule = rule
            end
        end
        return selectedRule
    elseif policy == resultCount
       for rule in rules
            selectedRule = temp[1]
            if length(rule.result) > length(selectedRule.result)
                selectedRule = rule
            end
        end
        return selectedRule
    elseif policy == firstRule
        return temp[1]
    elseif policy == ruleWithMoreRecentFacts
        return temp[1]
    elseif policy == random
        return temp[rand(1:length(listRules))]
    else
        return temp[rand(1:length(listRules))]
    end
end

function listValidRules(rules::KnowledgeBase, facts::Facts)::Set{Rule}
    validRules = Set{Rule}()

    for rule in rules
        if isRuleValid(rule, facts)
            push!(validRules, rule)
        end
    end

    return validRules
end

function isRuleValid(rule::Rule, facts::Facts)::Bool
    validPremiseCounter = 0

    for premise in rule.premise

        for fact in facts
            if isPremiseValid(premise, fact)
                validPremiseCounter += 1
                break
            end
        end

    end

    return validPremiseCounter == length(rule.premise)
end

function isPremiseValid(premise::Element, fact::Fact)::Bool
    if premise.variable == fact.variable
        if premise.operator == "<"
            if parse(Float64, fact.value) < parse(Float64, premise.value)
                return true
            end
        elseif premise.operator == ">"
            if parse(Float64, fact.value) > parse(Float64, premise.value)
               return true
            end
        elseif premise.operator == "="
            if fact.value == premise.value
                return true
            end
        elseif premise.operator <= "<="
            if parse(Float64, fact.value) == parse(Float64, premise.value)
                return true
            end
        elseif premise.operator == ">="
            if parse(Float64, fact.value) >= parse(Float64, premise.value)
                return true
            end
        elseif premise.operator == "<>"
            if fact.value != premise.value
                return true
            end
        else
            println("Unknown operator")
            return false
        end
    else
        return false
    end
    return false
end
