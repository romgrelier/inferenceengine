include("Util.jl")

function listValidRulesBackward(rules::KnowledgeBase, goals::Goals, search::Set{Element})::Set{Rule}
    validRules = Set{Rule}()

    for rule in rules
        if isRuleValidBackward(rule, goals)
            push!(validRules, rule)
        end
    end

    for rule in rules
        if isRuleValidBackward(rule, search)
            push!(validRules, rule)
        end
    end

    return validRules
end

function isRuleValidBackward(rule::Rule, goals::Goals)::Bool
    validPremiseCounter = 0

    for result in rule.result

        for goal in goals
            if isPremiseValid(goal, result)
                validPremiseCounter += 1
                break
            end
        end

    end

    return validPremiseCounter == length(rule.result)
end

function backwardChaining(knowledgeBase::KnowledgeBase, facts::Facts, goals::Goals, saturate::Bool, policy::ConflictPolicy)
    notFinished = true
    search = Set{Element}()

    # check if any goal can be validated
    for goal in goals
        for fact in facts
            if isPremiseValid(goal, fact)
                println("[goal reached : $(goal.variable)]")
                delete!(goals, goal)
                break
            end
        end

        if !saturate && length(goals) == 0
            return true
        end
    end

    iteration::Int64 = 0

    while notFinished
        notFinished = false
        iteration += 1

        println("[$(iteration)]")

        print("|\tfacts : ")
        for fact in facts
            print(fact)
        end
        println()

        print("|\tsearch : ")
        for premise in search
            print(premise)
        end
        println()

        validRules = listValidRulesBackward(knowledgeBase, goals, search)
        if length(validRules) == 0
            println("no rule available -STOP-")
            return false
        end
        println("|\tavailable rules :")
        for rule in validRules
            print("|\t\t")
            println(rule)
        end

        if length(validRules) >= 1
            notFinished = true
            factAdded = false
            noRuleAdded = true
            while noRuleAdded
                # apply policy in case of conflict
                choosenRule = chooseRuleToApply(validRules, policy)
                print("|\tchoosen rule : ")
                println(choosenRule)

                # if this rule is valid, let's add some facts
                if isRuleValid(choosenRule, facts)
                    for result in choosenRule.result
                        push!(facts, result)
                    end
                    delete!(knowledgeBase, choosenRule)
                    factAdded = true
                    noRuleAdded = true
                    println("facts added")
                    break
                end

                # add the premises that we need
                for premise in choosenRule.premise
                    noRuleAdded = false

                    # check if the premise does not exist already in search
                    for premiseSearch in search
                        if (premiseSearch.variable == premise.variable) & (premiseSearch.operator == premise.operator) & (premiseSearch.value == premise.value)
                            noRuleAdded = true
                        end
                    end

                    # check if the premise does not exist already in facts
                    for premiseFact in facts
                        if (premiseFact.variable == premise.variable) & (premiseFact.operator == premise.operator) & (premiseFact.value == premise.value)
                            noRuleAdded = true
                        end
                    end

                    if !noRuleAdded & !factAdded
                        noRuleAdded = false # we need to add at least on new premise to continue
                        push!(search, premise)
                    end

                end
                # we do not want to use a useless rule, let's choose another one
                if noRuleAdded
                    delete!(validRules, choosenRule)
                end
                if length(validRules) == 0
                    println("no more valid rule available")
                    return false
                end
            end

            # add validated premise from the search list as fact
            for fact in facts
                for premise in search
                    if isPremiseValid(premise, fact)
                        push!(facts, premise)
                        delete!(search, premise)
                    end
                end
            end

            # check if any goal can be validated
            for goal in goals
                for fact in facts
                    if isPremiseValid(goal, fact)
                        println("[goal reached : $(goal.variable)]")
                        delete!(goals, goal)
                        break
                    end
                end

                if !saturate && length(goals) == 0
                    return true
                end
            end

        end

        println()

    end
end
