import Base64.print
import Base64.println

struct Element
    variable::String
    operator::String
    value
end

function print(element::Element)
    print("$(element.variable) $(element.operator) $(element.value) ")
end

function println(element::Element)
    println("$(element.variable) $(element.operator) $(element.value)")
end

Fact = Element
Goal = Element
