include("Element.jl")

struct Rule
    premise::Array{Element, 1}
    result::Array{Element, 1}
end

function print(rule::Rule)
    for premise in rule.premise
        print(premise)
    end
    print("-> ")
    for result in rule.result
        print(result)
    end
end

function println(rule::Rule)
    print(rule)
    print("\n")
end
