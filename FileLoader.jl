include("KnowledgeBaseType.jl")

import Base64.+

@enum FileLoadStage begin
    rule = 1
    fact = 2
    goal = 3
    finished = 4
end

function +(stage::FileLoadStage, number::Int64)::FileLoadStage
    return FileLoadStage(Int64(stage) + number)
end

@enum RuleLoadStage begin
    premise = 1
    result = 2
end

@enum ElementLoadStage begin
    variable = 1
    operator = 2
    value = 3
end

function loadFromFile(filename)
    fileLoadStage = rule

    knowledgeBase::KnowledgeBase = KnowledgeBase()
    facts::Facts = Facts()
    goals::Goals = Goals()

    open(filename) do file

        # each line is a rule, a fact or a goal
        for line in eachline(file)

            # an empty line switch to the next stage
            if line == ""
                fileLoadStage += 1
                println(fileLoadStage)
                continue
            end

            if fileLoadStage == rule
                push!(knowledgeBase, lineToRule(line))
            elseif fileLoadStage == fact
                push!(facts, nextElement(line))
            elseif fileLoadStage == goal
                push!(goals, nextElement(line))
            elseif FileLoadStage == finished
                println("file loaded")
            end
        end

    end

    return (knowledgeBase, facts, goals)
end

function lineToRule(line::String)::Rule
    premises = []
    results = []

    newVariable = ""
    newOperator = ""
    newValue = ""

    ruleStage::RuleLoadStage = premise
    elementStage::ElementLoadStage = variable
    elementReady::Bool = false

    for e in split(line)
        if e == "&"
            continue
        end

        if e == "->"
            ruleStage = result
            continue
        end

        if elementStage == variable
            newVariable = e
            elementStage = operator
        elseif elementStage == operator
            newOperator = e
            elementStage = value
        elseif elementStage == value
            newValue = e
            elementStage = variable
            elementReady = true
        end

        if ruleStage == premise && elementReady
            push!(premises, Element(newVariable, newOperator, newValue))
            elementReady = false
        elseif ruleStage == result && elementReady
            push!(results, Element(newVariable, newOperator, newValue))
            elementReady = false
        end

    end

    return Rule(premises, results)
end

function nextElement(string::String)::Element
    newVariable = ""
    newOperator = ""
    newValue = ""

    elementReady = false
    elementStage::ElementLoadStage = variable

    for e in split(string)

        if elementStage == variable
            newVariable = e
            elementStage = operator
        elseif elementStage == operator
            newOperator = e
            elementStage = value
        elseif elementStage == value
            newValue = e
            elementStage = variable
            elementReady = true
        end

        if elementReady
            return Element(newVariable, newOperator, newValue)
        end

    end
end
